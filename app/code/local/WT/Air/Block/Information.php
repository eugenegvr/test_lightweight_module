<?php

class WT_Air_Block_Information extends Mage_Core_Block_Template
{
    protected function _construct()
    {
        parent::_construct();

        $this->setTemplate('wt_air/information.phtml');
    }
}
