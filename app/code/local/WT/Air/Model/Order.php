<?php

class WT_Air_Model_Order
{
    const PASSENGERS_CODES = [
        'adults'    => 'ADT',
        'children' => 'CHD',
        'infants'   => 'INF',
    ];

    public $errors = [];

    protected function checkResponse($raw)
    {
        if (isset($raw->Errors->Error)) {
            foreach ($raw->Errors->Error as $error) {
                $this->errors[] = [
                    'type' => (string)$error->attributes()['Type'],
                    'code' => (string)$error->attributes()['Code'],
                    'shortText' => (string)$error->attributes()['ShortText'],
                    'text' => (string)$error,
                ];
            }
            return false;
        }
        return true;
    }

    public $order_element = [];

    protected $passengers = [];

    public function load($id)
    {
        $readRequest = Mage::getModel('wt_air/readRequest');

        $orderRaw = $readRequest->getReservationById($id);

        if ($this->checkResponse($orderRaw)) {
            $this->parseReservationResponse($orderRaw);
        }
        return $this;
    }

    protected function parseReservationResponse(SimpleXMLElement $reservationRaw)
    {
        $this->parseDestinationOptions($reservationRaw);
        $this->parseTravelers($reservationRaw);
    }

    protected function parseDestinationOptions(SimpleXMLElement $reservationRaw)
    {
        $options = $reservationRaw
            ->AirReservation
            ->AirItinerary
            ->OriginDestinationOptions
            ->OriginDestinationOption;

        foreach ($options as $option) {
            $reservation_element = $option->FlightSegment;
            $flight_element_attributes = $reservation_element->attributes();

            $this->order_element[] = [
                'flightNumber' => (string)$flight_element_attributes['FlightNumber'],
                'departureDateTime' => (string)$flight_element_attributes['DepartureDateTime'],
                'arrivalDateTime' => (string)$flight_element_attributes['ArrivalDateTime'],
                'departureAirport' => (string)$reservation_element->DepartureAirport->attributes()['LocationCode'],
                'arrivalAirport' => (string)$reservation_element->ArrivalAirport->attributes()['LocationCode'],
            ];
        }
    }

    protected function parseTravelers(SimpleXMLElement $reservationRaw)
    {
        $passengers = $reservationRaw
            ->AirReservation
            ->TravelerInfo
            ->AirTraveler;

        foreach ($passengers as $passenger) {
            $this->passengers[] = [
                'passengerTypeCode' => (string)$passenger->attributes()['PassengerTypeCode'],
                'namePrefix' => (string)$passenger->PersonName->NamePrefix,
                'givenName' => (string)$passenger->PersonName->GivenName,
                'surname' => (string)$passenger->PersonName->Surname,
                'email' => (string)$passenger->Email,
                'gender' => (string)$passenger->attributes()['Gender'],
            ];
        }
    }


    public function getAgeCount()
    {
        $countArray = [];
        foreach (self::PASSENGERS_CODES as $key => $type) {
            foreach ($this->passengers as $passenger) {
                if ($passenger['passengerTypeCode'] === $type) {
                    $countArray[$key]++;
                }
            }
        }
        return $countArray;
    }
}
