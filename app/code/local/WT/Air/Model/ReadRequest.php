<?php

class WT_Air_Model_ReadRequest
{
    const AIR_API_CONFIG = [
        'endpoint' => 'https://test-api.worldticket.net/sso-gateway-service/sso/authorization/mservice',
        'grantType' => 'clientId',
        'clientId' => 'Ecommerce',
        'secret' => '9e76a221-ab29-4ffd-a615-6f91252d3f64',
        'username' =>'Ecommerce',
        'password' => 'EcomWT2016',
    ];

    protected function parseXml(string $responseXml)
    {
        try {
            try {
                return simplexml_load_string($responseXml)->children('ota', true);
            } catch (Exception $e) {
                throw new Exception(
                    Mage::helper('wt_air')->__('Failed to parse xml document: %s', $responseXml)
                );
            }
        } catch (Exception $e) {
            Mage::logException($e);

            return false;
        }
    }

    protected function createToken()
    {
        try {
            $url = self::AIR_API_CONFIG['endpoint'];

            $request = json_encode(self::AIR_API_CONFIG);

            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'Content-Length: ' . strlen($request)
            ]);

            $result = curl_exec($ch);

            curl_close($ch);

            $response = json_decode($result, true);

        } catch (Exception $e) {
            Mage::logException($e);
            return false;
        }

        $response['expiredAt'] = date(time() + $response['expiresIn']);

        Mage::getConfig()->saveConfig(
            'wt_air/access_token',
            json_encode($response),
            'default',
            0
        );

        return $response['tokenId'];
    }

    protected function getXMLRequestBuilder(string $rootTag)
    {
        $timeStamp = (new DateTime())->format(DateTime::ATOM);
        return new SimpleXMLElement(
            '<?xml version="1.0" encoding="UTF-8"?><'. $rootTag .' xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 ' . $rootTag . '.xsd"  EchoToken="456789" TimeStamp="'.$timeStamp.'" Target="Production" Version="2.001" SequenceNmbr="1" PrimaryLangID="en"/>'
        );
    }

    protected function doRequest(string $xmlRequest)
    {
        try {
            $tokenJson = Mage::getStoreConfig('wt_air/access_token');
            if ($tokenJsons) {
                $token = json_decode($tokenJson, true)['tokenId'];
            } else {
                $token = $this->createToken();
            }
            $url = 'https://test.worldticket.net/test-dat-ecom/ota-ecom-saml';
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/xml',
                'X-SalesChannel: COM',
                'Authorization: Bearer ' . $token,
            ]);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlRequest);

            $response = curl_exec($ch);

            curl_close($ch);
            return $response;

        } catch (Exception $e) {
            Mage::logException($e);

            return false;
        }
    }

    public function getReservationById(string $id)
    {
        $timeStamp = (new DateTime())->format(DateTime::ATOM);
        $xml = new SimpleXMLElement(
            '<?xml version="1.0" encoding="UTF-8"?><OTA_ReadRQ xmlns="http://www.opentravel.org/OTA/2003/05" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.opentravel.org/OTA/2003/05 OTA_ReadRQ.xsd"  EchoToken="456789" TimeStamp="'.$timeStamp.'" Target="Production" Version="2.001" SequenceNmbr="1" PrimaryLangID="en"/>'
        );

        $pos = $xml->addChild('POS');
        $source = $pos->addChild('Source');

        $source->addAttribute('ERSP_UserID', '1#Preved');
        $source->addAttribute('AgentSine', '2BB');
        $source->addAttribute('PseudoCityCode', 'ATL');
        $source->addAttribute('ISOCountry', 'US');
        $source->addAttribute('ISOCurrency', 'EUR');
        $source->addAttribute('AirlineVendorID', '1P');

        $requestorID = $source->addChild('RequestorID');
        $requestorID->addAttribute('Type', '5');
        $requestorID->addAttribute('ID', '35896241');

        $bookingChannel = $source->addChild('BookingChannel');
        $bookingChannel->addAttribute('Type', 'COM');

        $readRequest = $xml->addChild('ReadRequests')->addChild('ReadRequest');

        $uniqueID = $readRequest->addChild('UniqueID');
        $uniqueID->addAttribute('ID', $id);
        $uniqueID->addAttribute('Type', '14');

        $readRequest
            ->addChild('Verification')
            ->addChild('PersonName')
            ->addChild('Surname', 'AdultLastname');

        $responseXml = $this->doRequest($xml->asXML());

        if (!$responseXml) {
            return null;
        }

        return $this->parseXml($responseXml);
    }
}
