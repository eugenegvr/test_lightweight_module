<?php

class WT_Air_ShowController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }

    public function showAction()
    {
        $referenceID = $this->getRequest()->getParam('reference_id');
        $order = Mage::getModel('wt_air/order')->load($referenceID);
        $informationBlock = [ 'order' => [] ];

        if (!empty($order->errors)) {
            $informationBlock['order'] = [
                'errors' => $order->errors,
            ];
        } else {
            $informationBlock['order'] = [
                'flights' => $order->order_element,
                'passenger_summary' => $order->getAgeCount(),
            ];
        }

        $information = $this->getLayout()
            ->createBlock('wt_air/information', '', $informationBlock)
            ->toHtml();

        $this->getResponse()
            ->setHeader('Content-type','application/json',true)
            ->setBody(Mage::helper('core')->jsonEncode([
                'status' => 1,
                'information' => $information,
            ]));
    }
}
